# 说明

这个工程来自Spring官方的StomP样例，并将前端代码剔除（独立为 **demo** 工程），以模拟前后端分离的架构。

为了支持跨域访问，分别在 **CORSConfiguration** 和 **WebSocketMessageBrokerConfigurer** 的实现类 **WebSocketConfig** 里设置了 **CORS** 规则，分别用于 **SpringMVC** 和 **WebSocket** 访问。

如果前端使用 **Nginx代理** 实现跨域，则不需要这些设置。
