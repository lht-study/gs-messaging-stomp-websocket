package com.example.messagingstompwebsocket;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

@Configuration
public class CORSConfiguration extends WebMvcConfigurationSupport {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
/*         registry.addMapping("/**")
                // .exposedHeaders("Access-Control-Allow-Origin")
                .allowedOrigins("http://localhost", "http://localhost:80")
                .allowedMethods("*")
                .allowedHeaders("*")
                .allowCredentials(true);
 */        super.addCorsMappings(registry);
    }
 
}