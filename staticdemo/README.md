# README

StomP静态页面样例，这是从Spring的官方样例（前后端混合）项目抽取出来的。这是一个简单的前端工程，没有使用 **Vue** 、 **Webpack** 之类的技术，仅作为前后端分离形式的 **StomP** 通信样例。

由于这个项目需要跨域访问后端，因此依赖于跨域方案，使用的是Nginx代理形式。Nginx代理的配置在 **dev-env** 的Nginx配置里，访问的实际路径前加上 **/backend** 前缀，将对后端的请求转为localhost访问。

由于 **dev-env** 的Nginx是容器形式运行的，因此即使在本地测试时，Nginx代理的配置也不能将上游设置为 **localhost** ，因为对于Nginx来说，这个 **localhost** 是容器本身。

