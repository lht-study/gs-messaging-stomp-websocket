# 说明

这个工程是从官方的 **complete** 工程提取出来的，前端相关资源是使用 **web-jars** 加载的。

这个工程访问后端的跨域问题，是通过后端的 **CORS** 设置解决的，注意后端在设置 **CORS** 信息时，需要使用 **WebSocketMessageBrokerConfigurer** 实现类的 **registerStompEndpoints** 方法，并明确指定访问来源，不能使用 **`*`**，因为关于认证信息的 **allowCredentials** 属性为 **true**，这是一个限制。

最好的方法是使用 **Nginx** 代理，避免浏览器直接访问后端接口。

注意在设置Nginx代理时，如果Nginx运行在容器中，不能使用 **localhost** 作为上游的主机名，即使前后端实际上是在同一台服务器上运行，因为 **localhost** 被限制在容器内了。
